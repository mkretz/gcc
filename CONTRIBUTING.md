Contributions to this fork of GCC should generally follow the same
[conventions](https://gcc.gnu.org/codingconventions.html) as for the
upstream GCC project, and where appropriate, the specific conventions for
subsystems like
[libstdc++](https://gcc.gnu.org/onlinedocs/libstdc++/manual/appendix_contributing.html).

The main difference is that copyright assignment to the FSF is not required.
Instead, patches to this fork of GCC require the contributor to sign the
[Developer's Certificate of Origin](https://developercertificate.org/),
version 1.1, as reproduced in its entirety below.

To certify it, add a "Signed-off-by" line to your commit message, like this:

    Signed-off-by: Random J Developer <random@developer.example.org>

This will be done for you automatically if you use `git commit -s`.
Reverts should also include "Signed-off-by". `git revert -s` does that for you.

```
Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
1 Letterman Drive
Suite D4700
San Francisco, CA, 94129

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```
